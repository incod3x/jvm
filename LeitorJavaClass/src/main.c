//#################################################################################################
/*! \file main.c
 *
 * \brief Programa para fazer a leitura do ".class" e exibir suas informacoes
 */
//#################################################################################################

#include <stdio.h>
#include "../include/I_INTUSU.h"

int main (int argc, char* argv[]) {
    
    INTUSU_executar(argc, argv);

    return 0;
}